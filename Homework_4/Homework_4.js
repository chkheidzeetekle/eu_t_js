//kvirsi dgeebis kliki
function displayDayOfWeek(number) {
    const days = ['ორშაბათი', 'სამშაბათი', 'ოთხშაბათი', 'ხუთშაბათი', 'პარასკევი', 'შაბათი', 'კვირა'];

    if (number >= 1 && number <= 7) {
        const dayOfWeek = days[number - 1];
        document.getElementById('output').value = dayOfWeek;
        document.getElementById('displayDays').innerHTML = "<div>" + dayOfWeek + "</div>";
    } 
}
