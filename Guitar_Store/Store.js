const translations = {
    en: {
        title: 'Guitar Store',
        products: 'Our Products',
        acousticGuitar: 'Acoustic Guitar',
        acousticGuitarDesc: 'High quality acoustic guitar.',
        electricGuitar: 'Electric Guitar',
        electricGuitarDesc: 'Premium electric guitar.',
        bassGuitar: 'Bass Guitar',
        bassGuitarDesc: 'High quality bass guitar.',
        classicalGuitar: 'Classical Guitar',
        classicalGuitarDesc: 'Premium classical guitar.',
        price: 'Price: ',
        login: 'Log In',
        register: 'Register',
        sidebar: ['Acoustic Guitars', 'Electric Guitars', 'Bass Guitars', 'Accessories', 'Contact'],
        email: 'Email:',
        password: 'Password:',
        confirmPassword: 'Confirm Password:',
        passwordCriteria: 'Password must contain uppercase, lowercase letters, and numbers.',
        username: 'Username:',
        country: 'Country:',
        street: 'Street:',
        mobile: 'Mobile Number:',
        validation: {
            passwordCriteria: 'Password must contain at least 8 characters, including uppercase, lowercase letters, and numbers.',
            passwordsMatch: 'Passwords do not match.',
            registrationSuccess: 'Registration successful!'
        }
    },
    ge: {
        title: 'გიტარის მაღაზია',
        products: 'ჩვენი პროდუქტები',
        acousticGuitar: 'აკუსტიკური გიტარა',
        acousticGuitarDesc: 'მაღალი ხარისხის აკუსტიკური გიტარა.',
        electricGuitar: 'ელექტრო გიტარა',
        electricGuitarDesc: 'პრემიუმ ელექტრო გიტარა.',
        bassGuitar: 'ბას გიტარა',
        bassGuitarDesc: 'მაღალი ხარისხის ბას გიტარა.',
        classicalGuitar: 'კლასიკური გიტარა',
        classicalGuitarDesc: 'პრემიუმ კლასიკური გიტარა.',
        price: 'ფასი: ',
        login: 'შესვლა',
        register: 'რეგისტრაცია',
        sidebar: ['აკუსტიკური გიტარები', 'ელექტრო გიტარები', 'ბას გიტარები', 'აქსესუარები', 'კონტაქტი'],
        email: 'ელ.ფოსტა:',
        password: 'პაროლი:',
        confirmPassword: 'პაროლის დადასტურება:',
        passwordCriteria: 'პაროლმა უნდა შეიცავდეს მინიმუმ 8 სიმბოლოს, მათ შორის დიდი და პატარა ასოები და ციფრები.',
        username: 'მომხმარებლის სახელი:',
        country: 'ქვეყანა:',
        street: 'ქუჩა:',
        mobile: 'მობილური ნომერი:',
        validation: {
            passwordCriteria: 'პაროლი უნდა შეიცავდეს მინიმუმ 8 სიმბოლოს, მათ შორის დიდი და პატარა ასოები და ციფრები.',
            passwordsMatch: 'პაროლები არ ემთხვევა.',
            registrationSuccess: 'რეგისტრაცია წარმატებით დასრულდა!'
        }
    }
};

function switchLanguage(language) {
    document.documentElement.lang = language;
    document.getElementById('title').innerText = translations[language].title;
    document.querySelector('.section-title').innerText = translations[language].products;

    const products = document.querySelectorAll('.product');
    const productDetails = [
        { title: 'acousticGuitar', desc: 'acousticGuitarDesc', price: '200 ლარი' },
        { title: 'electricGuitar', desc: 'electricGuitarDesc', price: '500 ლარი' },
        { title: 'bassGuitar', desc: 'bassGuitarDesc', price: '300 ლარი' },
        { title: 'classicalGuitar', desc: 'classicalGuitarDesc', price: '400 ლარი' }
    ];

    products.forEach((product, index) => {
        const details = productDetails[index % 4];
        product.querySelector('.product-title').innerText = translations[language][details.title];
        product.querySelector('.product-description').innerText = translations[language][details.desc];
        product.querySelector('.product-price').innerText = translations[language].price + details.price;
    });

    document.getElementById('loginModalTitle').innerText = translations[language].login;
    document.getElementById('registerModalTitle').innerText = translations[language].register;

    document.querySelector('label[for="loginEmail"]').innerText = translations[language].email;
    document.querySelector('label[for="loginPassword"]').innerText = translations[language].password;

    document.querySelector('label[for="regEmail"]').innerText = translations[language].email;
    document.querySelector('label[for="regPassword"]').innerText = translations[language].password;
    document.querySelector('label[for="regConfirmPassword"]').innerText = translations[language].confirmPassword;
    document.getElementById('passwordCriteria').innerText = translations[language].passwordCriteria;
    document.querySelector('label[for="regUsername"]').innerText = translations[language].username;
    document.querySelector('label[for="regCountry"]').innerText = translations[language].country;
    document.querySelector('label[for="regStreet"]').innerText = translations[language].street;
    document.querySelector('label[for="regMobile"]').innerText = translations[language].mobile;

    document.getElementById('loginBtn').innerText = translations[language].login;
    document.getElementById('registerBtn').innerText = translations[language].register;

    const sidebarItems = document.querySelectorAll('.sidebar ul li');
    translations[language].sidebar.forEach((item, index) => {
        sidebarItems[index].innerText = item;
    });
}

const countryList = ["Germany", "France", "Georgia", "United States", "Canada", "Australia", "United Kingdom", "India"];
const countrySelect = document.getElementById('regCountry');

countryList.forEach(country => {
    const option = document.createElement('option');
    option.value = country;
    option.text = country;
    countrySelect.appendChild(option);
});

document.getElementById('registerForm').onsubmit = function(event) {
    const password = document.getElementById('regPassword').value;
    const confirmPassword = document.getElementById('regConfirmPassword').value;
    const language = document.documentElement.lang || 'en';
    const passwordCriteria = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{8,}$/;

    if (!passwordCriteria.test(password)) {
        alert(translations[language].validation.passwordCriteria);
        event.preventDefault();
        return false;
    }

    if (password !== confirmPassword) {
        alert(translations[language].validation.passwordsMatch);
        event.preventDefault();
        return false;
    }

    alert(translations[language].validation.registrationSuccess);
};

function closeModal(modalId) {
    document.getElementById(modalId).style.display = 'none';
}

document.getElementById('loginBtn').onclick = function() {
    document.getElementById('loginModal').style.display = 'block';
};

document.getElementById('registerBtn').onclick = function() {
    document.getElementById('registerModal').style.display = 'block';
};

window.onclick = function(event) {
    if (event.target.className === 'modal') {
        event.target.style.display = 'none';
    }
};

document.querySelector('.language-btn').addEventListener('click', function() {
    document.querySelector('.language-switch').classList.toggle('open');
});

function toggleLanguageMenu() {
    const menu = document.getElementById('languageMenu');
    menu.style.display = menu.style.display === 'block' ? 'none' : 'block';
}

// Closing language menu when clicking outside of it
window.onclick = function(event) {
    if (!event.target.matches('.language-btn')) {
        const dropdowns = document.getElementsByClassName('language-switch');
        for (let i = 0; i < dropdowns.length; i++) {
            const openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('open')) {
                openDropdown.classList.remove('open');
            }
        }
    }
};

document.addEventListener('DOMContentLoaded', (event) => {
    const slides = document.querySelectorAll('.image-slider .slide');
    let currentIndex = 0;

    setInterval(() => {
        currentIndex = (currentIndex + 1) % slides.length;
        document.querySelector('.image-slider .slides').style.transform = `translateX(-${currentIndex * 100}%)`;
    }, 5000);
});

